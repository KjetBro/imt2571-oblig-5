<?php

$file = "SkierLogs.xml";
$doc = new DOMDocument();
$doc->load( $file );

$xpath = new DOMXpath( $doc );

$db = null;
try {
	//Connect to the database
	$db = new PDO('mysql:host=localhost;dbname=skierlogs;charset=utf8mb4', 'root');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex) {
	echo "Database error";
}

$clubs = $xpath -> query("/SkierLogs/Clubs/Club");
foreach ( $clubs as $club ) {
	$id = $club -> getAttribute( "id" );
	$name = $club -> getElementsByTagName( "Name" )[0] -> nodeValue;
	$city = $club -> getElementsByTagName( "City" )[0] -> nodeValue;
	$county = $club -> getElementsByTagName( "County" )[0] -> nodeValue;
	try {
		$stmt = $db -> prepare("SELECT * FROM club WHERE id = ?");
		$stmt -> execute(array("$id"));
		$dbClub = $stmt -> fetch(PDO::FETCH_ASSOC);
		if ($dbClub['id'] == $id) {
			echo "Club with this id already exists\n";
		}
		else {
			try {
				$stmt = $db -> prepare("INSERT INTO club (id, clubName, city, county) VALUES (?, ?, ?, ?)");
				$stmt -> execute(array("$id", "$name", "$city", "$county"));
			}
			catch(PDOException $ex) {
				echo "Database error - clubs\n";
			}
		}
	}
	catch (PDOException $ex) {
		echo "Could not retrieve id from database\n";
	}
}

$skiers = $xpath-> query( "/SkierLogs/Skiers/Skier" );
foreach ( $skiers as $skier ) {
	$userName = $skier -> getAttribute( "userName" );
	$firstName = $skier -> getElementsByTagName( "FirstName" )[0] -> nodeValue;
	$lastName = $skier -> getElementsByTagName ( "LastName" )[0] -> nodeValue;
	$yearOfBirth = $skier -> getElementsByTagName ( "YearOfBirth" )[0] -> nodeValue;
	try {
		$stmt = $db -> prepare ("SELECT * FROM skier WHERE userName = ?");
		$stmt -> execute (array("$userName"));
		$dbSkier = $stmt -> fetch(PDO::FETCH_ASSOC);
		if ($dbSkier['userName'] == $userName) {
			echo "Skier with this username already exists\n";
		}
		else {
			try {
				$stmt = $db -> prepare("INSERT INTO skier (userName, firstName, lastName, yob) VALUES (?, ?, ?, ?)");
				$stmt -> execute(array("$userName", "$firstName", "$lastName", "$yearOfBirth"));
			}
			catch(PDOException $ex) {
				echo "Database error - skiers\n";
			}
		}
	}
	catch (PDOException $ex) {
		echo "Could not retrieve userName from database\n";
	}
}


$seasons = $xpath -> query( "/SkierLogs/Season" );
foreach ( $seasons as $season ) {
	$year = $season -> getAttribute( "fallYear" );
	try {
		$stmt = $db -> prepare ( "SELECT * FROM season WHERE year = ?");
		$stmt -> execute(array("$year"));
		$dbSeason = $stmt -> fetch(PDO::FETCH_ASSOC);
		if ($dbSeason['year'] == $year) {
			echo "Season with this year already exists\n";
		}
		else {
			try {
				$stmt = $db -> prepare( "INSERT INTO season (year) VALUES (?)" );
				$stmt -> execute(array( "$year" ));
			}
			catch (PDOException $ex) {
				echo  "Database error - seasons\n";
			}
		}
	}
	catch (PDOException $ex) {
		echo "Could not retrieve year from database\n";
	}
}


$seasons = $xpath -> query ("/SkierLogs/Season");
foreach ( $seasons as $season ) {
	$year = $season -> getAttribute("fallYear");
	try {
		$stmt = $db -> prepare ( "SELECT * FROM season WHERE year = ?");
		$stmt -> execute(array("$year"));
		$dbSeason = $stmt -> fetch(PDO::FETCH_ASSOC);
		if ($dbSeason['year'] == $year) {
			$clubs = $xpath -> query("/SkierLogs/Season[@fallYear='$year']/Skiers");
			foreach ( $clubs as $club ) {
				$clubId = $club -> getAttribute( "clubId" );
				try {
					$stmt = $db -> prepare("SELECT * FROM club WHERE id = ?");
					$stmt -> execute(array("$clubId"));
					$dbClub = $stmt -> fetch(PDO::FETCH_ASSOC);
					if ($dbClub['id'] == "" ) { 
						$clubId = null;
						$skiers = $xpath-> query( "/SkierLogs/Season[@fallYear='$year']/Skiers[not(@clubId)]/Skier" );
						foreach ( $skiers as $skier ) {
							$userName = $skier -> getAttribute( "userName" );
							try {
								$stmt = $db -> prepare ("SELECT * FROM skier WHERE userName = ?");
								$stmt -> execute (array("$userName"));
								$dbSkier = $stmt -> fetch(PDO::FETCH_ASSOC);
								if ($dbSkier['userName'] == $userName) {
									try {
										$stmt = $db -> prepare ("SELECT * FROM skier_club WHERE userName = ? AND year = ?");
										$stmt -> execute (array("$userName", "$year"));
										$dbSkierClub = $stmt -> fetch(PDO::FETCH_ASSOC);
										
										if ($dbSkierClub['userName'] == $userName && $dbSkierClub['year'] == $year) {
											echo "Skier_club relation already exists in the database\n";
										}
										else {
											try {
												$stmt = $db -> prepare("INSERT INTO skier_club (userName, id, year) VALUES (?, ?, ?)" );
												$stmt -> execute(array("$userName", "$clubId", "$year")); 
											}
											catch (PDOException $ex) {
												echo "Database error - club_skier\n";
											}
										}
									}
									catch (PDOException $ex) {
										echo "Could not retrieve username and year from database\n";
									}
								}
								else {
									echo "username not found in database\n";
								}
							}
							catch (PDOException $ex) {
								"Could not retrieve username from database\n";
							}
						}
					}
					else if ($dbClub['id'] == $clubId || $dbClub['id'] == "") {
						$skiers = $xpath-> query( "/SkierLogs/Season[@fallYear='$year']/Skiers[@clubId='$clubId']/Skier" );
						foreach ( $skiers as $skier ) {
							$userName = $skier -> getAttribute( "userName" );
							try {
								$stmt = $db -> prepare ("SELECT * FROM skier WHERE userName = ?");
								$stmt -> execute (array("$userName"));
								$dbSkier = $stmt -> fetch(PDO::FETCH_ASSOC);
								if ($dbSkier['userName'] == $userName) {
									try {
										$stmt = $db -> prepare ("SELECT * FROM skier_club WHERE userName = ? AND year = ?");
										$stmt -> execute(array("$userName", "$year"));
										$dbSkierClub = $stmt -> fetch(PDO::FETCH_ASSOC);
										if ($dbSkierClub['userName'] == $userName && $dbSkierClub['year'] == $year) {
											echo "Skier_club relation already exists in the database\n";
										}
										else {
											try {
												$stmt = $db -> prepare("INSERT INTO skier_club (userName, id, year) VALUES (?, ?, ?)" );
												$stmt -> execute(array("$userName", "$clubId", "$year")); 
											}
											catch (PDOException $ex) {
												echo "Database error - club_skier\n";
											}
										}
									}
									catch (PDOException $ex) {
										echo "Could not retrieve username and year from database\n";
									}
								}
								else {
									echo "username not found in database\n";
								}
							}
							catch (PDOException $ex) {
								"Could not retrieve username from database\n";
							}
						}
					}
					else {
						echo "Club id not found in database\n";
					}
				}
				catch (PDOException $ex) {
					"Could not retrieve club id from database\n";
				}
			}
		}
		else {
			echo "Year not found in database\n";
		}
	}
	catch (PDOException $ex) {
		"Could not retrieve year from database\n";
	}
}

$seasons = $xpath -> query ("/SkierLogs/Season");
foreach ( $seasons as $season ) {
	$year = $season -> getAttribute("fallYear");
	try {
		$stmt = $db -> prepare ( "SELECT * FROM season WHERE year = ?");
		$stmt -> execute(array("$year"));
		$dbSeason = $stmt -> fetch(PDO::FETCH_ASSOC);
		if ($dbSeason['year'] == $year) {
			$skiers = $xpath -> query("/SkierLogs/Season[@fallYear='$year']/Skiers/Skier");
			foreach ( $skiers as $skier ) {
				$userName = $skier -> getAttribute("userName");
				try {
					$stmt = $db -> prepare ("SELECT * FROM skier WHERE userName = ?");
					$stmt -> execute (array("$userName"));
					$dbSkier = $stmt -> fetch(PDO::FETCH_ASSOC);
					if ($dbSkier['userName'] == $userName) {
						try {
							$stmt = $db -> prepare ("SELECT * FROM log WHERE userName = ? AND year = ?");
							$stmt -> execute(array("$userName", "$year"));
							$dbLog = $stmt -> fetch(PDO::FETCH_ASSOC);
							if ($dbLog['userName'] == $userName && $dbLog['year'] == $year) {
								echo "Log relation already exists in the database\n";
							}
							else {
								try {
									$stmt = $db -> prepare ("INSERT INTO log (userName, year) VALUES (?, ?)");
									$stmt -> execute (array("$userName", "$year"));
								}
								catch(PDOException $ex) {
									echo "Database error - log\n";
								}
							}
						}
						catch (PDOException $ex) {
							echo "Could not retrieve username and year from the database\n";
						}
					}
					else {
						echo "Username was not found in database\n";
					}
				}
				catch (PDOException $ex) {
					echo "Could not retrieve username from the database\n";
				}
			}
		}
		else {
			echo "Year was not found in the database\n";
		}
	}
	catch (PDOException $ex) {
		echo "Could not retrieve year from the database\n";
	}
}


try {
	$stmt = $db -> prepare("SELECT * FROM log");
	$stmt -> execute();
	$dbLogs = array();
	while ($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
		$totalDistance = 0;
		$logId = $row['logId'];
		$year = $row['year'];
		$userName = $row['userName'];
		$entries = $xpath -> query ("/SkierLogs/Season[@fallYear='$year']/Skiers/Skier[@userName='$userName']/Log/Entry");
		foreach ( $entries as $entry ) {
			$date = $entry -> getElementsByTagName( "Date" )[0] -> nodeValue;
			$area = $entry -> getElementsByTagName( "Area" )[0] -> nodeValue;
			$distance = $entry -> getElementsByTagName( "Distance" )[0] -> nodeValue; 
			$totalDistance += $distance;
			try {
				$entryStmt = $db -> prepare("SELECT * FROM entry WHERE date = ? AND area = ? AND distance = ? AND logId = ?");
				$entryStmt -> execute(array("$date", "$area", "$distance", "$logId"));
				$dbEntry = $entryStmt -> fetch(PDO::FETCH_ASSOC);
				if ($dbEntry['date'] == $date && $dbEntry['area'] == $area && $dbEntry['distance'] == $distance && $dbEntry['logId'] == $logId) {
					echo "Entry already exists in the database\n";
				}
				else {
					try {
						$entryStmt = $db -> prepare("INSERT INTO entry(logId, date, area, distance) VALUES(?, ?, ?, ?)");
						$entryStmt -> execute(array("$logId", "$date", "$area", "$distance"));
					}
					catch(PDOException $ex) {
						echo "Database error - entry\n";
					}
				}
			}
			catch(PDOException $ex) {
				echo "Could not retrieve data from entry table\n";
			}
		}
		try {
			$distStmt = $db -> prepare("SELECT * FROM total_distance WHERE logId = ?");
			$distStmt -> execute(array("$logId"));
			$dbDist = $distStmt -> fetch(PDO::FETCH_ASSOC);
			if ($dbDist['logId'] == $logId){
				echo "Total_distance relation already exists\n";
			}
			else {
				try {
					$distStmt = $db -> prepare("INSERT INTO total_distance (logId, totalDistance) VALUES (?, ?)");
					$distStmt -> execute(array("$logId", "$totalDistance"));
				}
				catch (PDOException $ex) {
					echo "Database error - totalDistance\n";
				}
			}
		}
		catch(PDOException $ex) {
			echo "Could nt retrieve logId from total_distance table\n";
		}
	}
}
catch (PDOException $ex) {
	echo "Could not retrieve logs from database\n";
}
?>